/*
    SPDX-FileCopyrightText: 2006-2007 Kevin Ottens <ervin@kde.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#ifndef SOLID_MANAGERBASE_P_H
#define SOLID_MANAGERBASE_P_H

#include <QMutex>
#include <QObject>

#include "solid/solid_export.h"

namespace Solid
{
class BackendManagerPrivate
{
public:
    BackendManagerPrivate();
    virtual ~BackendManagerPrivate();

    /**
     * @threadsafe
     *
     * return a list of all registered backends
     */
    QList<QObject *> managerBackends() const;

private:
    void loadBackends();
    QList<QObject *> m_backends;
    mutable QMutex m_mutex;
};
}

#endif
